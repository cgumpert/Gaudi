!-----------------------------------------------------------------------------
! Package     : RootCnv
! Responsible : Markus Frank
! Purpose     : Root based persistency
! Commit Id   : $Format:%H$
!-----------------------------------------------------------------------------

================================ RootCnv v2r2 ================================

! 2016-03-10 - commit 4a18175

 - removed uses of templates implements[1-4], extends[1-4] and
   extend_interfaces[1-4]

   Since merge request !22 they are unnecessary.
    See merge request !133
================================ RootCnv v2r1 ================================

! 2015-11-02 - commit 57f356c

 - Merge branch 'hive' into 'master'

   Fixes GAUDI-978.

   See merge request !65


! 2015-10-26 - commit de80db5

 - More modernization changes

   Fix (almost) all warnings from clang 3.7, and use clang-modernize to further
   modernize the code.

   Fixes GAUDI-1118.

   See merge request !49


! 2015-10-15 - commit 30d16f1

 - Fixed GAUDI-1109: RootCnv not compiling with ROOT 6.02.08

   Added inclusion of `algorithm`.

   See merge request !51

================================ RootCnv v2r0 ================================

! 2015-09-25 - commit 35dd00c

 - Merge branch 'dev-smartif-use' into 'master'

   Provide (and use) C++11 smart pointer 'look and feel' for SmartIF

   The aim of this branch is to confine, for everything that inherits from
   IInterface, the calls to addRef(), release() and queryInterface() to the
   SmartIF implementation. Exceptions are a few places where interfaces
   (currently) return bare pointers (instead of SmartIF...) and where one thus
   has to addRef() explicitly to avoid returning a dangling pointer. This can be
   avoided by changing the relevant interface to return a SmartIF instead of a
   bare pointer.

   In addition, make SmartIF 'look and feel' like a smart pointer.

   - use explict bool conversion instead of .isValid()
   - add SmartIF::as<IFace>(), to return a SmartIF<IFace> to an alternate
      interface -- which (together with move) encourages the use of auto
   - add ISvcLocator::as<IFace>(), to return a SmartIF<IFace> to the current
   ISvcLocator.
   - add ServiceManager::service<IFace>() which return SmartIF<IFace> which
   encourages
      the use of auto

   And add a few other C++11 modernizations (eg. prefer STL over raw loop)

   Fixes GAUDI-1094

   See merge request !24


! 2015-09-11 - commit c062cbe

 - C++11 modernization changes

   Some trivial - and some not so trivial! - changes which take advantage of
   C++11...

   See merge request !7

============================== RootCnv v1r23p3 ===============================
! 2015-05-21 - Marco Clemencic
 - Fixed compilation with CMT.

! 2015-03-14 - Marco Clemencic
 - GAUDI-1024: Replaced GaudiKernel/Tokenizer with the new AttribStringParser.

============================== RootCnv v1r23p2 ===============================
! 2014-10-10 - Marco Clemencic
 - Fixed warning about redeclared macro when building the latest Gaudi with
   ROOT 5.

! 2014-10-10 - Marco Clemencic
 - Removed PoolDbNTupleDescriptor from genreflex selection file.

============================== RootCnv v1r23p1 ===============================
! 2014-09-19 - Marco Clemencic
 - Merged changes between Gaudi RootCnv (commit 8583adc) and LHCb
   Online/RootCnv (r176626).

============================== RootCnv v1r22p2 ===============================
! 2014-06-19 - Marco Clemencic
 - Minor fix for unpredictable VERBOSE messages (message suppression not
   correctly implemented).

! 2014-06-05 - Marco Cattaneo
 - GAUDI-962: fix unprotected debug messages.

============================== RootCnv v1r22p1 ===============================
! 2014-03-18 - Ben Couturier
 - Fixed bug #104127: remove hwaf configuration.

================================ RootCnv v1r22 ================================
! 2013-12-16 - Marco Clemencic
 - Clean-up of some leftover from the autoloading workaround.

! 2013-12-11 - Sebastien Binet
 - Added hwaf configuration files.

! 2013-11-15 - Marco Clemencic
 - Made the data member of pool::Token transient (it was implicit in ROOT 5, but
   must be explicit in ROOT 6).
   See https://sft.its.cern.ch/jira/browse/ROOT-5700

! 2013-11-13 - Marco Clemencic
 - Removed work-around for the autoloading problem.
 - Made PoolClasses.h a public header (needed for relocatability of dictionaries).

! 2013-10-03 - Marco Clemencic
 - Work-around for ROOT 6 autoloading problem (genreflex rootmap is incomplete).

! 2013-10-03 - Marco Clemencic
 - Removed dynamic loading of libCintex.so in RootIOHandler (ROOT 6 only).

! 2013-08-27 - Marco Clemencic, Danilo Piparo
 - Removed unneeded dependency on libCintex.so.

! 2013-07-19 - Marco Clemencic
 - Clean up in the use of the new PluginService.

! 2013-07-18 - Marco Clemencic
 - Removed use of obsolete AlgFactory.h, SvcFactory.h, ToolFactory.h,
   AudFactory.h and CnvFactory.h.

! 2013-07-03 - Marco Clemencic
 - Replaced Reflex queries with TClass queries.
 - Replaced obsolete 'gROOT->GetClass' with 'TClass::GetClass'.
 - Fixed gcc 4.7 narrowing conversion warnings.

============================= RootCnv v1r21 =================================
============================ RootCnv v1r20p1 ================================
! 2013-07-08 - Marco Clemencic
 - Fixed dependency problem with event extractor.

! 2013-07-05 - Marco Clemencic
 - Updated CMake configuration.

!========================= RootCnv v1r20 2013-05-23 =========================
! 2013-04-25 - Markus Frank
 - Fix compiler warnings for gcc47

! 2012-11-22 - Markus Frank
 - Add event extractor

!========================= RootCnv v1r19 2012-11-16 =========================
! 2012-11-16 - Marco Clemencic
 - Added CMake configuration file.

! 2012-11-14 - Markus Frank
 - Add incident "CorruptedInputFile" if the data or reference branch
   of a given file cannot be read (aka ROOT returns -1 on read).

! 2012-11-07 - Marco Clemencic
 - Fixed warning: format '%X' expects argument of type 'unsigned int*',
                  but argument N has type 'int*' [-Wformat]

!========================= RootCnv v1r18 2012-09-28 =========================
! 2012-09-24 - Markus Frank
 - Change optimization options to be part of the RootCnvSvc and not part of
   the RootDataConnection + adapt all called functions to use these.

!========================= RootCnv v1r17 2012-09-16 ========================
! 2012-09-16 - Markus Frank
 - Add developments from Ivan Valencik for memory optimization.
   Options to modify the default behaviour:
   RootCnvSvc.BasketSize = 2*MBYTE
   RootCnvSvc.BufferSize = 2*kBYTE
   RootCnvSvc.SplitLevel = 0
   Ivan also developed a small component to measure process quantities
   such as the memory usage and cpu consumption.

!========================= RootCnv v1r13 2012-02-23 ========================
! 2012-02-23 - Markus Frank
 - Add option to RootCnvSvc to change compression factor in the following form:
   "<compression-algorithm>:<compression-level>"
   Currently supported algorithms are: LZMA and ZLIB with levels 1-9
   Example:
   RootCnvSvc.GlobalCompression     = "LZMA:6";  // First algorithm, then level
   The default is "LZMA:6", to disable any compression set:
   RootCnvSvc.GlobalCompression     = "0";

   Please note:
   Since setting individual options for all different conversion services
   (Event, FSR,...) is a major pain and in general only leads to confusion,
   this is a GLOBAL option. You set it once and it will be valid for any
   subsequent call to TFile::Open in ANY RootDataConnection objects.


!========================= RootCnv v1r12 2012-01-24 ========================
! 2012-01-20 - Markus Frank
 - Fix gcc46 compile warning in merge.C

!========================= RootCnv v1r11 2012-01-11 ========================
! 2011-11-11 - Markus Frank
 - Remove RootOutputStream component. It's not used.

!========================= RootCnv v1r10 2011-11-07 ========================
! 2011-10-31 - Markus Frank
 - Fix bug #88049: RootCnv has problems reading POOL XDST
   For unknown reasons some pool files have a funny file version number.
   Explicitly handle this case.

!========================= RootCnv v1r9 2011-10-11 =========================
! 2011-10-11 - Markus Frank
 - Fix bug for programming the branch cache properly

!========================= RootCnv v1r8 2011-09-15 =========================
! 2011-09-13 - Markus Frank
 - Fix bug when writing FSRs for files with no events

!========================= RootCnv v1r6 2011-06-14 =========================
! 2011-06-14 - Marco Cattaneo
 - Add release notes file......
