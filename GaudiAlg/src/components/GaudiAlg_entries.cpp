#include "GaudiAlg/EventCounter.h"
#include "GaudiAlg/Prescaler.h"
#include "GaudiAlg/Sequencer.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiAlg/GaudiSequencer.h"
#include "GaudiAlg/GaudiAtomicSequencer.h"

DECLARE_COMPONENT( EventCounter   )
DECLARE_COMPONENT( Prescaler      )
DECLARE_COMPONENT( Sequencer      )
DECLARE_COMPONENT( GaudiSequencer )
DECLARE_COMPONENT( GaudiAtomicSequencer )
